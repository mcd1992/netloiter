# netloiter

Tool used for discovery of non-routable / 'hidden' networks on a shared layer 2 segment.

## Description

Often times network devices (switches, iDRAC, UPSs, cameras) will have a management network / IP assigned to all interfaces and is discoverable. This tool will listen in on target networks and attempt to contact hosts with known default IPs. This tool could be used during penetration tests for discovering pivot points or other vulnerable devices.

## Known devices / networks
- D-Link DWS switches - Use 10.90.90.90 for default management interface
- Dell iDRAC - Use 192.168.0.120 for default management
- https://github.com/vanhauser-thc/thc-hydra/blob/master/dpl4hydra_full.csv

## Notes
For now the basic idea is to send Duplicate-Address-Discovery style ARP requests for known default management IPs. Then we can just listen in on an interface promisciously for the ARP replies. Might run into issues with devices that dont handle ARP requests with DAD style sender-ip and target MAC addresses properly.

Have I been beaten to the punch: https://github.com/royhills/arp-scan

`nping -e enp4s0 --arp --arp-type arp --arp-sender-ip 0.0.0.0 --arp-target-mac ff:ff:ff:ff:ff:ff --arp-target-ip 10.90.90.90 10.90.90.90`

## Todo
- 'ARP stuffing' support (APC UPSs seem to use this)
- Support CIDR ranges for brute-forcing instead of a list
